/* Wstawione poniżej zmienne to tzw. zmienne globalne. Zmienne globalne są dostępne z każdego poziomu skryptu
niezależnie od funkcji, która się do nich odwołuje. Zminne deklarowane w funkcjach nie posiadają tego typu
właściwości - przestają istnieć wraz z zakończeniem ciała funkcji (ograniczają je nawiasy klamrowe).

Zmienna mojaLiczba będzie przechowywać nam wartość gdy użytkownik kliknie dowolny operator arytmetyczny (np. +,-). Pełni ona rolę podręcznej pamięci i 
daje możliwość usunięcia z ekranu obecnie wyświetlanej wartości. 
Zmienna czyscLCD jest wartością prawda/fałsz (boolean); gdy jej wartość wynosi true ekran ma zostać wyczyszczony.

PRZYKŁAD: Użytkownik wpisuje wartości 32 po czym klika znak +. W tym momencie zmienna mojaLiczba zmienia swoją wartość na 32. Zmienna czyscLCD zmienia swoją 
wartość na true. Przy wpisaniu dowolnej cyfry (np. 1) wyświetlacz zostanie wyczyszczony (z wartości 32) i wyświetli się tylko nowo wpisana cyfra. Jeżeli klikniemy
znak równości otrzymamy wynik dodawania dwóch wprowadzonych wartości.

UWAGA! Wersja prezentacyjna kalkulatora jest ograniczona (demo). W wyniku działań mogą brać udział jedynie cyfry 1,2,3 oraz jedyne dostępne działanie - dodawanie.
Pozostałą funkcjonalność należy dodać SAMEMU!!!

Więcej: https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Polecenia/var lub https://www.w3schools.com/js/js_variables.asp
 */
var mojaLiczba = 0;
var czyscLCD = false;

/***********************************************************************************************************************************************************
PONIŻSZE TRZY FUNKCJE OPISANE ZOSTANĄ W JEDNYM KOMENTARZU PONIŻEJ. KOMENTARZ OBEJMUJE KOD ZAMKNIĘTY W ZNAKACH GWIAZDKI!!

Poniższe funkcje odpowiadają kolejno za operacje wyświetlenia cyfry 9,8 oraz 7. Każda z tych funkcji przypisana jest do odpowiedniego przycisku w 
kodzie HTML (atrybut onclick). Każda funkcja wynajduje w kodzie element z identyfikatorem #wyswietlacz. Ponieważ zawiera on dodatkowy element (paragraf),
który realnie wyświetla cyfry, musimy się do niego dostać. W tym celu wykorzystujemy zmienną tablicową children, którą posiada KAŻDY element HTML w drzewku DOM.
Tablica ta skupia w sobie wszystkie zależne od wybranego elementu inne elementy HTML. W tym wypadku posiada dokładnie jeden taki element - paragraf (znajduje się
on pod indeksem w tablicy o numerze 0).
Następnie wybieramy właściwość naszego paragrafu o nazwie innerHTML - pozwala ona na odczyt jak i zmianę aktualnie zawartych wewnątrz wybranego elementu wartości.
innerHTML przyjmuje tekst, potrafi także interpretować i tworzyć na tej podstawie nowe elememnty HTML (wewnątrz obecnie edytowanego).

Więcej: https://developer.mozilla.org/pl/docs/Web/JavaScript/Guide/Funkcje lub https://www.w3schools.com/js/js_function_definition.asp lub https://www.w3schools.com/js/js_functions.asp
*/
function pokazZnak9() {
	//alert('Wciśnięto 9!'); //pozostałość po sprawdzaniu czy przycisk działą poprawnie. Wyświetla okienko z podanym tekstem
	/*poniższy fragment kodu sprawdza czy innerHTML zawiera tekst równy wartości '0'. Jeżeli tak jest element innerHTML jest czyszczony 
	(wstawiana jest pusta wartość) 
	
	Więcej: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/if...else lub https://www.w3schools.com/js/js_if_else.asp
	
	
	Więcej (na temat wynajdywania elementów): https://developer.mozilla.org/en-US/docs/Web/API/HTMLFormElement/elements lub https://www.w3schools.com/Js/js_htmldom_elements.asp
	*/
	if (document.getElementById('wyswietlacz').children[0].innerHTML == '0')
		document.getElementById('wyswietlacz').children[0].innerHTML = '';
	/*niezależnie od instrukcji if do wartości innerHTML dodana zostaje cyfra 9 (za każdym kliknięciem)*/
	document.getElementById('wyswietlacz').children[0].innerHTML = document.getElementById('wyswietlacz').children[0].innerHTML + '9';
}

function pokazZnak8() {
	//alert('Wciśnięto 8!');
	/*w poprzedniej funkcji każdorazowo używana była funkcja odnajdująca getElementById. Nie jest to dobre rozwiązanie - każde jej wywołanie powoduje
	PRZESZUKANIE DOKUMNETU OD POCZĄTKU CELEM ZNALEZIENIE TEGO SAMEGO ELEMENTU (zła efektywność). Dlatego lepszym pomysłem jest "uchwycenie" potrzebnego
	nam elementu i odwoływanie się do niego jako zmiennej. Dzięki temu JavaScript "pamięta" nasz element (a dokładnie jego lokalizację w pamięci RAM),
	przez co nie musi go ciągle szukać. W większych dokumentach znacznie przyspiesza to działanie skryptów
	UWAGA! Poniższy kod nie zadziała poprawnie. Co prawda będziemy mieli dostęp do zawartości ekranu, a sama zmienna lcd będzie 
	zmieniana to jednak należy mieć na uwadze, że JavaScript nie zrobi odwołania do elementu A DO TEKSTU Z innerHTML!! Błąd ten naprawa następna funkcja!*/
	var lcd = document.getElementById('wyswietlacz').children[0].innerHTML;
	console.log('Zaraz po uchwyceniu obiektu ' + lcd);
	if (lcd == '0')
		lcd = '';
	console.log('Element po wykonaniu if ' + lcd);//funkcja pozwala wyświetlić stan zawartości zmiennej lcd i zobaczyć iż ma ona zmienianą zawartość
	
	/*poniższa linia zostanie zmieniona w następnej funkcji - WAŻNE*/
	lcd = lcd + '8';
	
	console.log('Element po dodaniu zawartości ' + lcd);//jak wyżej
}

function pokazZnak7() {
	//alert('Wciśnięto 7!');
	/*Poprawnie wykonany uchwyt do ELEMENTU HTML, nie zaś do ZAWARTOŚCI WŁAŚCIWOŚCI*/
	var lcd = document.getElementById('wyswietlacz').children[0];
	console.log('Zaraz po uchwyceniu obiektu ' + lcd);
	if (lcd.innerHTML == '0')
		lcd.innerHTML = '';
	console.log('Element po wykonaniu if ' + lcd);
	/*poniżej widać możliwy skrót zapisu dodawania nowej wartości do istniejącej zawartości.
	Zamiast pisać 
	lcd.innerHTML = lcd.innerHTML + '7'
	można zapisać dokładnie tą samą operację poprzez operator +=. Interpreter wie wtedy, że ma zachować obecną wartość zmiennej/właściwości po lewej stronie
	i dodać odpowiednią zawartość z prawej strony (w naszym wypadku literał '7').
	Prócz dodawania można jeszcze skracać:
	- odejmowanie - -=
	- mnożenie - *=
	- dzielenie - /=
	- reszty z dzielenie (modulo) - %=
	
	Istnieją jeszcze inne operatory przypisania, więcej można dowiedzieć się z treści spod adresu: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Assignment_Operators*/
	lcd.innerHTML += '7';
}
/* KONIEC OPISU DZIAŁANIA FUNKCJI
**************************************************************************************************************************************************************/


/* Przyciski 6,5 oraz 4 działają w oparciu o poniższą, jedną funkcję. Rozwiązanie pozwala na:
- zaoszczędzenie przestrzeni kodu
- co znacznie zwiększa czytelność kodu
- przyśpiesza ładowanie strony (mniej kodu do przetworzenia)
- w przypadku błędów i problemów łatwiejsze ich eliminowanie (brak potrzeby dokonywania tych samych zmian w wielu miejscach)

Funkcja przyjmuje jeden parametr (o nazwie znak), którego wartość ustawiana jest w chwili wywoływania funkcji (bezpośrednio w atrybucie onclick).
Reszta funkcji wygląda tak samo jako wcześniejsze - przypisujemy do zmiennej lcd obiekt paragrafu z #wyswietlacz (zmienna-uchwyt),
czyścimy zawartość ekranu jeżeli zawiera on pojedyncze zero, a następnie, niezależnie od warunku,
przypisujemy do zawartości wyświetlacza przekazaną wartość przez parametr znak*/
function pokazZnak(znak) {
	var lcd = document.getElementById('wyswietlacz').children[0];
	if (lcd.innerHTML == '0')
		lcd.innerHTML = '';
	lcd.innerHTML += znak;
}

/*ostatnia funkcja, prawdopodobnie najbardziej uniwersalna na obecny poziom programowania. Pozwala ona przypisywanie do JAKIEGOKOLWIEK przycisku
dokładnie tego samego zdarzenia z tą samą funkcją, która będzie przekazywać dokładnie tą samą wartość (this).

INFORMACJA: this jest specjalnym rodzajem zmiennej. Nie odnosi się do żadnej konkretnej wartości lecz to obecnego stanu WYWOŁUJĄCEGO OBIEKTU. Oznacza to, że 
w chwili przekazania this do naszej funkcji pod parametrem przycisk będzie uchwyt do przycisku, którego wciśnięcie wywowało funkcję dodanZnak. Dzięki temu
nie jest wymagane ciągłe wpisywanie wartości do wyświetlenia - będzie ona pobierana bezpośrednio z zawartości przycisku

Więcej: https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Operatory/this lub https://www.w3schools.com/js/js_this.asp
*/
function dodajZnak(przycisk) {
	//console.log(przycisk);
	var lcd = document.getElementById('wyswietlacz').children[0];
	if (lcd.innerHTML == '0' || czyscLCD == true) {
		lcd.innerHTML = '';
		czyscLCD = false;
	}
	/*poniższy warunek sprawdza czy innerText (zmienna podobna do innerHTML, jednak w przeciwieństwie do innerHTML zawiera jedynie czysty tekst wybranego
	elementu, wycinając wszystkie elementy i atrybuty HTML
	do sprawdzenia, czy innerText zawiera znak '+' wykorzystana została metoda badania ciągów znakowych includes, która nie bada czy ciągi są równe 
	(zakomentowany warunek linikę niżej) lecz czy ciąg przycisk.innerText ZAWIERA pokady inny ciąg znakowy (w tym wypadku +)
	
	UWAGA! Zmiana została dokonana celem naprawy funkcjonalności dla przeglądarki Opera, Chrome (poprzednie rozwiązanie działało dla Firefox)
	
	Więcej: https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Obiekty/Array/includes lub https://www.w3schools.com/jsref/jsref_includes.asp
	*/
	if (przycisk.innerText.includes('+')) {
	//if (przycisk.innerText == '+') {
		mojaLiczba = lcd.innerHTML;
		czyscLCD = true;
	}
	else if (przycisk.innerText.includes('=')) {
	//else if (przycisk.innerText == '=') {
		/*poniższa funkcja parseInt() zamienia ciąg znakowy w liczbę całkowitą.
		
		Należy PAMIĘTAĆ(!), że w językach programowania WSZYSTKO jest tylko serią/ciągiem zer i jedynek. To programista musi sam wskazać w jaki sposób traktować
		owe ciągi - czy mają być traktowane jak liczba, znak/znaki drukarskie, wartości prawdy i fałszu czy też bardziej rozbudowane struktury (np. tablice - 
		jak to ma miejsce w przypadku zmiennej children). JavaScript automatycznie potrafi przypisywać typy danych do używanych zmiennych. Niestety jeżeli JS 
		przypisze zmiennej typ ciągu znakowego to nie będzie można go dodać do innej wartości (np. liczby). Stąd niekiedy zachodzi potrzeba jawnego wskazania JS
		iż mamy chęć np. traktować daną zmienną jako inny typ (w tym wypadku jako liczbę). Oczywiście trzeba pamiętać, że zmienne muszą dać się przemienić na inne
		(nie da się np. zamienić ciągu "Witaj" na liczbę, ale "34,5" już tak). 
		
		Więcej: https://www.w3schools.com/js/js_number_methods.asp lub https://developer.mozilla.org/pl/docs/Web/JavaScript/Guide/Obsolete_Pages/Przewodnik_po_j%C4%99zyku_JavaScript_1.5/Funkcje_predefiniowane/Funkcje_parseInt_i_parseFloat
		*/
		lcd.innerHTML = parseInt(lcd.innerText) + parseInt(mojaLiczba);
		czyscLCD = true;
		mojaLiczba = 0;
	}
	else {
		/*UWAGA! Przeglądarki inne niż Firefox ŹLE INTERPRETUJĄ zawartość innerText. Mają tendencję do wstawiania pustych znaków/znaków nowej linii. 
		Powoduje to późniejeszze problemy z interpretacją ciągu znakowego na liczbę (wybierana jest pierwsza cyfra, reszta jest kasowana). DLatego
		poniższa linia została zmieniona tak by wykorzystać innerHTML - zostało dodane odwołanie do paragrafu zawartego w przycisku (odwołaniem jest
		tablica children[0])
		
		Więcej o innerText: https://developer.mozilla.org/en-US/docs/Web/API/Node/innerText lub https://www.w3schools.com/jsref/prop_node_innertext.asp
		
		Więcej o innerHTML: https://developer.mozilla.org/pl/docs/Web/API/Element/innerHTML lub https://www.w3schools.com/jsref/prop_html_innerhtml.asp
		*/
		//lcd.innerHTML = przycisk.innerText;
		lcd.innerHTML += przycisk.children[0].innerHTML;
	}
}